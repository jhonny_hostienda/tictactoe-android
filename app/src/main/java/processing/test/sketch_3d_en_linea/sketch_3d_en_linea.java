package processing.test.sketch_3d_en_linea;

import processing.core.PApplet;
import android.view.MotionEvent;

import java.util.ArrayList;

import ketai.ui.*;
import apwidgets.*;
import shapes3d.Toroid;

public class sketch_3d_en_linea extends PApplet implements OnClickWidgetListener{
    ArrayList<Celda> celdas;
    ArrayList<X_O> xos;

    KetaiGesture gesture;
    float lastx = 0;
    float lasty = 0;
    int ancho = Global.ancho;
    int largo = Global.largo;
    int separacion = 200;
    double velArrastre = 4;
    private float sumarx;
    private float sumary;

    PWidgetContainer widgetContainer;
    PButton botonViewMode;
    private boolean isViendo;
    private float escala;

    Toroid shapeO;
    final int tamOX = (int) (Global.ancho*0.25);


    public void setup()
    {
        escala=1;
        isViendo=false;
        celdas=new ArrayList<Celda>();
        xos=new ArrayList<X_O>();

        gesture = new KetaiGesture(this);
        //Turn on smoothing to make everything pretty.
        smooth();
        stroke(64, 64, 64);
        fill(127, 127, 127, 150);
        //Tell the rectangles to draw from the center point (the default is the TL corner)
        rectMode(CENTER);

        widgetContainer = new PWidgetContainer(this); //create new container for widgets
        botonViewMode = new PButton(10, 10, "Modo Vista"); //create new button from x- and y-pos. and label. size determined by text content
        //button2 = new PButton(10, 60, 100, 50, "Bigger"); //create new button from x- and y-pos., width, height and label
        widgetContainer.addWidget(botonViewMode); //place button in container
        // widgetContainer.addWidget(button2); //place button in container


        // Help for Toroid(PApplet app, int nbrTubeSegments, int nbrLengthSegments)
        shapeO = new Toroid(this,5, 25);
        // Help  for setRadius(float tubeX, float tubeY, float ring)
        // Set the tube radii as well as the ring radius.
        shapeO.setRadius(15, 15,(tamOX/2)-20);
        shapeO.rotateToX(radians(90));
        shapeO.fill(color(0,96,5));
    }

    float angleX = 0;
    float angleY = 0;

    public void draw()
    {
        background(220);
        //se centra
        translate(width / 2, height / 2);
        //se dibuja la "caja"
        pushMatrix();

        //se calculan movimientos
        if(isViendo)
        {
            calcularMovimiento();
        }
        scale (escala);
            //se rota al angulo resultante
            rotateY(angleY);
            rotateX(angleX);
        drawXOs();
        insertCube();
        //se borra la "caja"
        popMatrix();
    }

    private void drawXOs()
    {
        for(int i=0;i<xos.size();i++)
        {
           if((xos.get(i).plano>=getPlano())||(isViendo))//si la "x" u "o" esta en el nivel actual o niveles menores o esta en modo vista| pues se dibuja
            {
                xos.get(i).draw();
            }

        }
    }

    public void calcularMovimiento()
    {
        float difx = mouseX - lastx;
        float dify = mouseY - lasty;
        if (difx > velArrastre) {
            sumarx += (PI / width);
        } else if (difx < -velArrastre)
        {
            sumarx -= (PI / width);
        } else if (dify > (velArrastre))
        {
            sumary -= (PI / height);
        } else if (dify < -(velArrastre)) {
            sumary += (PI / height);
        } else {
            sumarx = 0;
            sumary = 0;
        }
        lastx = mouseX;
        lasty = mouseY;
        angleY += sumarx;
        angleX += sumary;
    }

    public void insertCube()
    {
        //ultimo plano
        llevarA(3);
        if(planosActivos>=1)
        {
            insertPlano();
        }
        //plano central
        llevarA(2);
        if(planosActivos>=2)
        {
            insertPlano();
            //se dibuja el indicador de no activo
            rect(0, 0, (ancho / 5)*2, (largo / 5)*2);
        }
        //primer plano
        llevarA(1);
        if(planosActivos>=3)
        {
            insertPlano();
        }
    }

    public void llevarA(int nivel)
    {
        switch(nivel)
        {
            case 1:
                //primer plano
                translate(0, 0, separacion);break;
            case 2:
                //plano central
                translate(0, 0, separacion);break;
            case 3://ultimo plano
                translate(0, 0, -separacion);
        }
    }


    public void insertPlano()
    {
        for(int i=0;i<9;i++)
        {
            celdas.add(new Celda(separacion,5,5,5));
        }
        //verticales
        line(-ancho / 5, -largo / 2, -ancho / 5, largo / 2); //x1,y1,x2,y2
        line(ancho / 5, -largo / 2, ancho / 5, largo / 2);
        //horizontales
        line(-ancho / 2, -largo / 5, ancho / 2, -largo / 5);
        line(-ancho / 2, largo / 5, ancho / 2, largo / 5);
        rect(0, 0, ancho, largo);

    }

    public String sketchRenderer() {
        return P3D;
    }

    int planosActivos=3;
    public boolean surfaceTouchEvent(MotionEvent event)
    {
        //call to keep mouseX, mouseY, etc updated
        super.surfaceTouchEvent(event);
        //forward event to class for processing
        return gesture.surfaceTouchEvent(event);
    }


    public void onDoubleTap(float x, float y)
    {

    }

    public void onTap(float x, float y)
    {
        if(!isViendo)
        {
            xos.add(new X_O(celdas.get(0),x,y,getPlano(),1));//inserta una x
        }
    }



    public void onLongPress(float x, float y)
    {
        if(!isViendo)
        {
            xos.add(new X_O(celdas.get(0),x,y,getPlano(),0));//inserta una o
        }

    }

    private int getPlano() {
        switch(planosActivos)
        {
            case 1: return separacion;//lo manda al ultimo pplano
            case 2: return  0;//lo manda al plano del medio
            case 3: return  -separacion;//lo deja en el plano superior
        }
        return 0;
    }

    //the coordinates of the start of the gesture,
// end of gesture and velocity in pixels/sec
    public void onFlick( float x, float y, float px, float py, float v)
    {
        if(!isViendo)
        {
            if (y<py)
            {
                if (planosActivos != 1)
                {
                    planosActivos--;
                }   // a swipe downwards
            }
            else
            {
                if (planosActivos != 3)
                {
                    planosActivos++;
                }  // a swipe upwards
            }
        }
    }

    public void onPinch(float x, float y, float d)
    {
        if(isViendo)
        {
            if(((escala>=0.5)&&(d<0))||((escala<=2)&&(d>0)))
            {
                escala+=d/200;
            }

        }
    }

    @Override
    public void onClickWidget(PWidget pWidget)
    {
        if(pWidget == botonViewMode)
        { //if it was button1 that was clicked
            if(botonViewMode.getText().equals("Modo Juego"))
            {
                angleX=0;
                angleY=0;
                escala=1;
                isViendo=false;
                botonViewMode.setText("Modo Vista");
            }else
            {
                planosActivos=3;
                isViendo=true;
                botonViewMode.setText("Modo Juego");
            }
        }
    }


    class X_O
    {
        private Celda celda;
        private float x;
        private float y;
        private int plano;
        private int tipo;

        public X_O(Celda celda,float x,float y,int plano,int tipo)
        {
           this.x=x-(width/2);
           this.y=y-(height/2);
           this.celda=celda;
           this.plano=plano;
           this.tipo=tipo;
        }

        public void draw()
        {
            pushStyle();
            switch(tipo)
            {
                case 1://X
                    paintX();break;
                case 0://O
                    paintO();break;
            }
            popStyle();
        }

        void paintX ()
        {
            // red
            fill(color(137,1,1));
            noStroke();
            translate(x,y,-plano);
            pushMatrix();
            rotateZ(radians(45));
            box (tamOX,25,25);
            popMatrix();

            pushMatrix();
            rotateZ(radians(-45));
            box (tamOX,25,25);
            popMatrix();
            noStroke();
            translate(-x,-y,plano);
        }

        void paintO()
        {
                translate(x,y,(-plano));
                shapeO.draw();
                translate(-x, -y, (plano));
        }
    }
}